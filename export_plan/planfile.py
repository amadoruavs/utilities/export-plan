"""
Planfile utility for writing a QGroundControl .plan file.
"""

from typing import List
import json

MAV_CMD_NAV_LOITER_UNLIM = 17
MAV_CMD_DO_SET_HOME = 179
MAV_CMD_DO_SET_ROI_LOCATION = 195
MAV_CMD_IMAGE_START_CAPTURE = 2000

# Gimbal protocol v1
MAV_CMD_DO_MOUNT_CONTROL = 205
MAV_MOUNT_MODE_MAVLINK_TARGETING = 2


class Planfile:
    """
    Planfile utility class for writing a QGroundControl .plan file.
    """
    def __init__(self, filepath: str, home_lat: float, home_lon: float, default_speed: float=15, ASML: bool=True):
        """
        Create a Planfile class for writing to a QGC .plan file.

        :param filepath: path to QGC .plan file you want to write
        :param home_lat: home latitude of the vehicle
        :param home_lon: home longitude of the vehicle
        :param ASML    : sets the frame of the mission to above mean sea level if True, else above vehicle home altitude (default: True)
        """
        self.filepath = filepath
        self.fileptr = open(filepath, "w")

        if ASML:
            self.altitude_mode = 2
            self.frame = 0
        else:
            self.altitude_mode = 1
            self.frame = 3

        self.plandata = {
            "fileType": "Plan",
            "version": 1,
            "geoFence": {
                "circles": [],
                "polygons": [],
                "version": 2
            },
            "rallyPoints": {
                "points": [],
                "version": 2
            },
            "groundStation": "QGroundControl",
            "mission": {
                "plannedHomePosition": [
                    home_lat,
                    home_lon,
                    50
                ],
                "vehicleType": 2,
                "version": 2,
                "cruiseSpeed": 15,
                "firmwareType": 12,
                "globalPlanAltitudeMode": self.altitude_mode,
                "hoverSpeed": default_speed,
                "items": [
                ]
            }
        }


    def add_loiter(self, lat, lon, altitude):
        self.plandata["mission"]["items"].append({
            "AMSLAltAboveTerrain": altitude,
            "Altitude": altitude,
            "AltitudeMode": self.altitude_mode,
            "autoContinue": True,
            "command": MAV_CMD_NAV_LOITER_UNLIM,
            "doJumpId": 29,
            "frame": self.frame,
            "params": [
                0,
                0,
                0,
                None,
                lat,
                lon,
                altitude
            ],
            "type": "SimpleItem"
        })

    def add_loiter_time(self, lat, lon, altitude, time):
        self.plandata["mission"]["items"].append({
            "AMSLAltAboveTerrain": altitude,
            "Altitude": altitude,
            "AltitudeMode": self.altitude_mode,
            "autoContinue": True,
            "command": 19,
            "doJumpId": 29,
            "frame": self.frame,
            "params": [
                time,
                1,
                0,
                1,
                lat,
                lon,
                altitude
            ],
            "type": "SimpleItem"
        })

    def add_circular_geofence(self, lat, lon, radius, inclusion):
        """
        Stationary obstacles are represented by an exclusionary circular geofence
        """
        self.plandata["geoFence"]["circles"].append({
            'circle': {
                'center': [ lat, lon ],
                'radius': radius
            },
            'inclusion': inclusion,
            'version': 1
        })

    def set_home_position(self, lat, lon, height=0):
        """
        Lost Comms Position is represented by a home position
        """
        self.plandata["mission"]["items"].append({
            "AMSLAltAboveTerrain": height,
            "Altitude": height,
            "AltitudeMode": self.altitude_mode,
            "autoContinue": True,
            "command": MAV_CMD_DO_SET_HOME,
            "doJumpId": 29,
            "frame": self.frame,
            "params": [
                0,
                None,
                None,
                None,
                lat,
                lon,
                height
            ],
            "type": "SimpleItem"
        })

    def ROI(self, lat, lon, height=0):
        """
        ROI's are used to represent offAxisOdlcPos and emergentLastKnownPos
        """
        self.plandata["mission"]["items"].append({
            "AMSLAltAboveTerrain": height,
            "Altitude": height,
            "AltitudeMode": self.altitude_mode,
            "autoContinue": True,
            "command": MAV_CMD_DO_SET_ROI_LOCATION,
            "doJumpId": 29,
            "frame": self.frame,
            "params": [
                0,
                None,
                None,
                None,
                lat,
                lon,
                height
            ],
            "type": "SimpleItem"
        })

    def add_geofence(self, points: List[List[float]], inclusion: bool=True):
        """
        Add a geofence polygon.

        :param points: a list of waypoints in the form
                       [lat, lon] (e.g. [[lat1, lon1], [lat2, lon2]])

        :param inclusion: whether to enable this geofence in QGC (default: True)
        """
        self.plandata["geoFence"]["polygons"].append({
            "inclusion": inclusion,
            "polygon": points,
            "version": 1
        })

    def add_geofence_points(self, geofence_index: int, points: List[List[float]]):
        """
        Add points to a geofence polygon at the specified index.

        :param geofence_index: Index of geofence to add points to
        :param points: Points in the form [lat, lon] to add to geofence
        """
        for point in points:
            self.plandata["geoFence"]["polygons"][geofence_index].append(point)

    def set_geofence_inclusion(self, index: int, inclusion: bool):
        """
        Set a geofence's inclusion parameter to True or False.

        :param index: Index of geofence to enable/disable
        :param inclusion: New status of geofence (inclusion=True, disabled=False)
        """
        self.plandata["geoFence"]["polygons"][index]["inclusion"] = inclusion

    def remove_geofence(self, index: int):
        """
        Remove a geofence at the specified index.

        :param index: Index of geofence to remove
        """
        self.plandata["geoFence"]["polygons"].pop(index)

    def insert_waypoint(self, index: int, lat: float, lon: float, height: float, pass_radius: float=0, yaw: float=None, image_capture: bool=False):
        """
        Inserts a waypoint at the specified point in the mission plan.

        :param index: Index to insert waypoint at
        :param lat: Latitude of waypoint
        :param lon: Longitude of waypoint
        :param height: Altitude of waypoint in meters MSL
        :param image_capture: If True triggers a start image capture command at the waypoint (Default=False)
        """
        self.plandata["mission"]["items"].insert(index, {
            "AMSLAltAboveTerrain": height,
            "Altitude": height,
            "AltitudeMode": self.altitude_mode,
            "autoContinue": True,
            "command": 16,
            "doJumpId": 29,
            "frame": self.frame,
            "params": [
                0,
                0,
                pass_radius,
                yaw,
                lat,
                lon,
                height
            ],
            "type": "SimpleItem"
        })

        if image_capture:
            self.plandata["mission"]["items"].insert(index + 1, {
                "autoContinue": True,
                "command": MAV_CMD_IMAGE_START_CAPTURE,
                "doJumpId": 29,
                "frame": 2,
                "params": [
                    0,
                    0,
                    1,
                    0,
                    None,
                    None,
                    None
                ],
                "type": "SimpleItem"
            })


    def add_waypoint(self, lat: float, lon: float, height: float, pass_radius: float=0, yaw: float=None, image_capture: bool=False):
        """
        Add a waypoint to the end of the mission plan.

        :param lat: Latitude of waypoint
        :param lon: Longitude of waypoint
        :param height: Altitude of waypoint in meters MSL
        :param image_capture: If True triggers a start image capture command at the waypoint (Default=False)
        """
        self.insert_waypoint(len(self.plandata["mission"]["items"]),
                             lat, lon, height, pass_radius, yaw, image_capture)


    def add_change_speed(self, speed: float):
        self.plandata["mission"]["items"].append({
            "autoContinue": True,
            "command": 178,
            "doJumpId": 29,
            "frame": 2,
            "params": [
                1,
                speed,
                -1,
                0,
                0,
                0,
                0
            ],
            "type": "SimpleItem"
        })

    def insert_gimbal_tilt(self, index: int, pitch: float, roll: float, yaw: float):
        """
        Inserts a gimbal tilt command at the specified point in the mission plan.

        :param index: Index to insert survey at
        :param pitch: Pitch of tilt in degrees
        :param roll: Roll of tilt in degrees
        :param yaw: Yaw of tilt in degrees
        """
        self.plandata["mission"]["items"].insert(index, {
            "autoContinue": True,
            "command": MAV_CMD_DO_MOUNT_CONTROL,
            "doJumpId": 29,
            "frame": 2,
            "params": [
                pitch,
                roll,
                yaw,
                0,
                0,
                0,
                MAV_MOUNT_MODE_MAVLINK_TARGETING
            ],
            "type": "SimpleItem"
        })


    def add_gimbal_tilt(self, pitch: float, roll: float, yaw: float):
        """
        Adds a gimbal tilt command to the end of the mission plan.

        :param pitch: Pitch of tilt in degrees
        :param roll: Roll of tilt in degrees
        :param yaw: Yaw of tilt in degrees
        """

        self.insert_gimbal_tilt(len(self.plandata["mission"]["items"]), pitch, roll, yaw)

    def insert_survey(self, index: int, points: List[List[float]]):
        """
        Add an area survey mission item to the mission plan at the specified index.

        :param index: Index to insert survey at
        :param points: points in the form [lat, lon], forming the boundary for the survey area
        """
        self.plandata["mission"]["items"].insert(index, {
            "TransectStyleComplexItem": {
                "CameraCalc": {
                    "AdjustedFootprintFrontal": 25,
                    "AdjustedFootprintSide": 25,
                    "CameraName": "Manual (no camera specs)",
                    "DistanceToSurface": 50,
                    "DistanceToSurfaceRelative": True,
                    "version": 1
                },
                "CameraShots": 59,
                "CameraTriggerInTurnAround": True,
                "FollowTerrain": False,
                "HoverAndCapture": False,
                "Items": [],
                "Refly90Degrees": False,
                "TurnAroundDistance": 10,
                "VisualTransectPoints": [
                ],
                "version": 1
            },
            "angle": 0,
            "complexItemType": "survey",
            "entryLocation": 0,
            "flyAlternateTransects": False,
            "polygon": points,
            "type": "ComplexItem",
            "version": 4,
        })

    def add_survey(self, points: List[List[float]]):
        """
        Add an area survey mission item to the end of the mission plan.

        :param points: points in the form [lat, lon], forming the boundary for the survey area
        """
        self.insert_survey(len(self.plandata["mission"]["items"]),
                           points)

    def remove_mission_item(self, index: int):
        """
        Remove a mission item at the specified index (waypoint or area survey).

        :param index: Index of mission item to remove
        """
        self.plandata["mission"]["items"].pop(index)

    def geofences(self) -> List[List[float]]:
        """
        Returns all current geofences.
        """
        return self.plandata["geoFence"]["polygons"]

    def mission_items(self) -> List[dict]:
        """
        Returns all current mission items.
        """
        return self.plandata["mission"]["items"]

    def write(self):
        """
        Write everything to .plan file.
        """
        json.dump(self.plandata, self.fileptr)

    def close(self):
        """
        Close the file.
        """
        self.write()
        self.fileptr.close()
