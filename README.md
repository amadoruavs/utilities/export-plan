# qgcwrite

Python library for creating simple .plan waypoint files for visualization.

Usage:

```python
from qgcwrite import Planfile

lat = 39
lon = -121

planner = Planfile("output.plan", lat, lon)
planner.add_waypoint(39, -122)
planner.insert_waypoint(0, 39, -122)
planner.add_survey([39, -122], [39, -123], [40, -122], [40, -123])
planner.remove_mission_item(0)
planner.close()
```
