from setuptools import setup, find_packages

setup(
    name = "export_plan",
    version = "0.1.0",
    author = "Vincent Wang",
    author_email = "vwangsf@gmail.com",
    classifiers=[
        "Topic :: Utilities",
    ],
    packages=find_packages()
)
